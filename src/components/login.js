import React from 'react';
import {Grid,Paper,Avatar,TextField,Button,Typography,Link,InputLable,Input } from '@material-ui/core'
import LockSharpIcon from '@material-ui/icons/LockSharp';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';


const paperStyle={ border:'solid black',padding:20, height:'93vh',width:280,margin:" auto",backgroundColor:'white',display:'box'}
const Avatarstyle={backgroundColor:'rgba(0,0,0,0.2)', margin:"30px auto"}
const ButtonStyle={margin:'10px 0px'}
class Login extends React.Component
{
  constructor(props) {
    super(props)
    this.state = {
      hidden: true,
      username: '',
      password:''
    }
    this.myChangeHandler = this.myChangeHandler.bind(this)
 this.handlePasswordChange = this.handlePasswordChange.bind(this);
 this.toggleShow = this.toggleShow.bind(this);
  }
  myChangeHandler = (evt) =>   
  {
    
    this.setState({ [evt.target.name]: evt.target.value });
    this.setState({ password: evt.target.value });
};
toggleShow(e) {
  e.preventDefault();
  this.setState({ hidden: !this.state.hidden });
}
componentDidMount() {
  if (this.props.password) {
    this.setState({ password: this.props.password });
  }
}
  onlogin=(event)=> {
    if(this.state.username && this.state.password){
    alert("Hello " +this.state.username+"\n You have succefully Login  ");
  }
  else{
    alert("Please Input Valid Details  ")
  }
}

// handlePasswordChange(e) {
   
//     this.setState({ password: e.target.value });
//   }
//   toggleShow(e) {
//     e.preventDefault();
//     this.setState({ hidden: !this.state.hidden });
//   }
//   componentDidMount() {
//     if (this.props.password) {
//       this.setState({ password: this.props.password });
//     }
//   }
    render(){
    return(
        <Grid >
                  <Paper elevation={10} style={paperStyle}>
                     <Grid align='center' margin=''> 
                          <Avatar style={Avatarstyle}>
                             <LockSharpIcon/>
                            </Avatar>
                          <h2>Sign In</h2>
                      </Grid> 
                      <TextField label='Username'name="username" placeholder='Enter UserName'
                      onChange={this.myChangeHandler} fullWidth required/>
                     <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
          <Input
            id="standard-adornment-password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={myChangeHandler('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
          />
                      {/* <TextField label='Password' name="password" placeholder='Enter Your Password'
                     onChange={this.myChangeHandler}  fullWidth required/> */}
                    
                      <FormControlLabel
        control={
          <Checkbox
            name="checked"
            color="primary"
          />
        }
        label="Remember Me"
      />
      <Button type='submit' variant="contained" color='primary'style={ButtonStyle} fullWidth onClick={this.onlogin} >Login</Button>
      <Typography> <Link href="#" >
                        Forgot Password?
                    </Link>
                    </Typography>
                     <Typography> Don't have an account?
                         <Link href="sign up" >
                         Sign Up
                    </Link>
                    </Typography>
            </Paper>
        </Grid>
    )
}
}

export default Login;